<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    protected $table = "reservations";
    /**
     * Users that belongs to the reservation
     */
    public function hostUser()
    {
        return $this->belongsTo('App\User', 'host');
    }

    public function guestUsers()
    {
        return $this->hasMany('App\ReservationGuest');
    }

}
