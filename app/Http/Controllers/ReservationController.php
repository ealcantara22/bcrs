<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\ReservationGuest;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations = Reservation::with(array('hostUser','guestUsers'))->get();
        $reservations = json_decode($reservations->toJson());
        return response()->json(['code' => 200, 'status' => 'ok', 'message'=> 'success', 'response' => $reservations ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // main store logic
        return $this->handleReservationCreation($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param Reservation $reservation
     * @return \Illuminate\Http\Response
     * @internal param $id
     * @internal param Reservation $reservation
     */
    public function show(Reservation $reservation)
    {
        $reservation = Reservation::with(array('hostUser','guestUsers'))->find($reservation->id);
        if (is_null($reservation)){
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message'=> 'error',
                'response' => array('errorCode'=> 404, 'errorMessage'=>'Not Found')], 404);
        }
        $reservation = json_decode($reservation->toJson());
        return response()->json(['code' => 200, 'status' => 'ok', 'message'=> 'success', 'response'=> $reservation], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function updateNotRequired(Request $request, Reservation $reservation)
    {
        // -- Not Required
    }

    /**
     * Delete guest from host user active reservation
     * @param $hostId
     * @param $guestId
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeHostGuest($hostId, $guestId)
    {
        # -- host user information
        $hostUser = User::find(intval($hostId));
        if (!$hostUser) {
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message' => 'error',
                'response' => array('errorCode' => 404, 'errorMessage' => 'Host User Not Found')], 404);
        }
        # -- guest user information
        $guestUser = User::find(intval($guestId));
        if (!$guestUser) {
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message' => 'error',
                'response' => array('errorCode' => 404, 'errorMessage' => 'Guest User Not Found')], 404);
        }

        # -- active host user reservation
        $reservation = Reservation::where('active', 1)->where('host', $hostUser->id)->first();
        if (!$reservation) {
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message' => 'error',
                'response' => array('errorCode' => 404, 'errorMessage' => 'No Active Reservation Found')], 404);
        }

        $reservationGuest = ReservationGuest::where('reservation_id', $reservation->id)->where('user_id', $guestUser->id)->delete();
        if (!$reservationGuest) {
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message' => 'error',
                'response' => array('errorCode' => 404, 'errorMessage' => 'Guest Does Not Exist in Host Reservation.')], 404);
        }

        return response()->json(['code' => 200, 'status' => 'ok', 'message'=> 'success', 'response'=> true], 200);
    }

    /**
     * Handle the new reservation creation process
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    private function handleReservationCreation($data){
        /** @var User $hostUser */
        $hostUser = null;
        $guests = new Collection();

        /**
         * check if is a valid users structure
         */
        if (array_key_exists('users', $data) == false || is_array($data['users']) == false){
            return response()->json(['code' => 400, 'status' => 'Bad Request', 'message'=> 'error',
                'response' => array('errorCode'=> 400, 'errorMessage'=>'Bad Request')], 400);
        }

        /**
         * how many users exist in the array
         */
        if (sizeof($data['users']) == 0){
            return response()->json(['code' => 400, 'status' => 'Bad Request', 'message'=> 'error',
                'response' => array('errorCode'=> 400, 'errorMessage'=>'Bad Request')], 400);
        }

        /**
         * parsing users id's
         */
        foreach ($data['users'] as $key => $value){
            if (array_key_exists('id', $value)) {
                $id = $value['id'];
                if ($key == 0) {
                    $hostUser = User::find(intval($id));
                    if (!$hostUser) {
                        return response()->json(['code' => 400, 'status' => 'Bad Request', 'message' => 'error',
                            'response' => array('errorCode' => 400, 'errorMessage' => 'Bad Request')], 400);
                    }
                    if ($hostUser->is_host == false) {
                        return response()->json(['code' => 400, 'status' => 'Bad Request', 'message' => 'error',
                            'response' => array('errorCode' => 400, 'errorMessage' => 'Bad Request')], 400);
                    }
                } else {
                    $guest = User::find(intval($id));
                    if (!$guest) {
                        return response()->json(['code' => 400, 'status' => 'Bad Request', 'message' => 'error',
                            'response' => array('errorCode' => 400, 'errorMessage' => 'Bad Request')], 400);
                    }
                    # check if exists in the collection
                    $found = false;
                    foreach ($guests as $g) {
                        if ($g->id == $guest->id) {
                            $found = true;
                            break;
                        }
                    }
                    if ($found == false)
                        $guests->add($guest);
                }
            }else{
                return response()->json(['code' => 400, 'status' => 'Bad Request', 'message' => 'error',
                    'response' => array('errorCode' => 400, 'errorMessage' => 'Bad Request')], 400);
            }
        }

        # -- checking if the host user has an active reservation
        $reservation = Reservation::where('active', 1)->where('host', $hostUser->id)->first();
        if ($reservation){
            return response()->json(['code' => 400, 'status' => 'Bad Request', 'message'=> 'error',
                'response' => array('errorCode'=> 400, 'errorMessage'=>'Bad Request')], 400);
        }

        # -- persisting new reservation
        $reservation = new Reservation();
        $reservation->host = $hostUser->id;
        $reservation->active = ((array_key_exists('active', $data) && $data['active'] == true) ? true : false);

        DB::beginTransaction();
        try {
            $reservation->save();
            /** @var User $guest */
            foreach ($guests as $guest){
                $rg = new ReservationGuest();
                $rg->reservation_id = $reservation->id;
                $rg->user_id = $guest->id;
                $rg->save();
            }
            DB::commit();
            return response()->json(['code' => 201, 'status' => 'ok', 'message'=> 'success', 'response' => json_decode($reservation->toJson())], 201);
        }catch (\Exception $e){
            DB::rollBack();
            return response()->json(['code' => 500, 'status' => 'Internal Server Error', 'message'=> 'error',
                'response' => array('errorCode'=> 500, 'errorMessage'=>'Internal Server Error')], 500);
        }
    }
}
