<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\ReservationGuest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $users = json_decode($users->toJson());
        return response()->json(['code' => 200, 'status' => 'ok', 'message'=> 'success', 'response'=> $users], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // crating a new user
        $data = $request->all();
        try{
            $user = User::create($data);
            $user = json_decode($user->toJson());
            return response()->json(['code' => 201, 'status' => 'created', 'message'=> 'success', 'response'=> $user], 201);
        }catch (\Exception $e){
            return response()->json(['code' => 500, 'status' => 'Internal Server Error', 'message'=> 'error',
                'response' => array('errorCode'=> 500, 'errorMessage'=>'Internal Server Error')], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = json_decode($user->toJson());
        return response()->json(['code' => 200, 'status' => 'OK', 'message'=> 'success', 'response'=> $user], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->all();
        try {
            $user->update($data);
            $user = json_decode($user->toJson());
            return response()->json(['code' => 200, 'status' => 'ok', 'message' => 'success', 'response' => $user], 200);
        }catch (\Exception $e){
            return response()->json(['code' => 500, 'status' => 'Internal Server Error', 'message'=> 'error',
                'response' => array('errorCode'=> 500, 'errorMessage'=>'Internal Server Error')], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            /** remove where user is guest */
            ReservationGuest::where('user_id', $user->id)->delete();
            /** remove where user is host */
            Reservation::where('host', $user->id)->delete();
            /** removing the user */
            $user->delete();
            return response()->json(['code' => 200, 'status' => 'OK', 'message' => 'success', 'response' => true], 200);
        }catch (\Exception $e){
            return response()->json(['code' => 500, 'status' => 'Internal Server Error', 'message'=> 'error',
                'response' => array('errorCode'=> 500, 'errorMessage'=>'Internal Server Error')], 500);
        }
    }

    /**
     * Return the guests list by user
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @internal param $id
     */
    public function getUserGuests(User $user)
    {
        # active reservation by user
        $reservation = Reservation::where('active', 1)->where('host', $user->id)->first();
        if (is_null($reservation)){
            return response()->json(['code' => 404, 'status' => 'Not Found', 'message'=> 'error',
                'response' => array('errorCode'=> 404, 'errorMessage'=>'Not Found')], 404);
        }
        # list of guest by reservation
        $guests = ReservationGuest::with('user')->where('reservation_id', $reservation->id)->get();
        $guests = json_decode($guests->toJson());
        return response()->json(['code' => 200, 'status' => 'ok', 'message' => 'success', 'response' => $guests], 200);
    }
}
