<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationGuest extends Model
{
    //
    protected $table = "reservation_guests";

    protected $hidden = array('user_id', 'created_at', 'updated_at');

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
