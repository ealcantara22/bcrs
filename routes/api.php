<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

# -- user controller routes
Route::resource('users', 'UserController');
Route::get('users/{id}/guests', 'UserController@getUserGuests');

# -- reservations controller routes
Route::resource('reservations', 'ReservationController');
Route::delete('reservations/hosts/{hostId}/guests/{guestId}', 'ReservationController@removeHostGuest');
